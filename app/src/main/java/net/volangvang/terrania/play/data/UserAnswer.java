package net.volangvang.terrania.play.data;

import com.google.gson.annotations.SerializedName;

public class UserAnswer {

    @SerializedName("answer")
    private int answer;
    public UserAnswer(int answer) {
        this.answer = answer;
    }

    public int getAnswer() {
        return answer;
    }
}
