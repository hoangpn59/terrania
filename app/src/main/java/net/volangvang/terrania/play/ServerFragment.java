package net.volangvang.terrania.play;


import android.os.Bundle;

import net.volangvang.terrania.play.server.Server;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


public class ServerFragment extends Fragment {

    private Server server;
    private String mode;
    private int score = 0;
    private String gameID;
    private String continent;
    private boolean local;

    public ServerFragment() {
        // Required empty public constructor
    }

    void setServer(Server svr) {
        server = svr;
    }

    Server getServer() {
        return server;
    }

    String getMode() {
        return mode;
    }

    public int getScore() {
        return score;
    }

    int bumpScore() {
        return ++score;
    }

    void setMode(String mode) {
        this.mode = mode;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    boolean isLocal() {
        return local;
    }

    void setLocal(boolean local) {
        this.local = local;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    String getGameID() {
        return gameID;
    }

    void setGameID(String id) {
        this.gameID = id;
    }
}
