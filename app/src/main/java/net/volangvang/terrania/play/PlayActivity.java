package net.volangvang.terrania.play;

import android.os.Bundle;
import android.preference.PreferenceManager;

import net.volangvang.terrania.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PlayActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.play_view_pager)
    ViewPager viewPager;
    private String[] gameModes = {};
    private String[] gameModeValues = {};
    private String[] gameModeDescriptions = {};
    private boolean offline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        offline = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("offline", true);
        gameModes = getResources().getStringArray(R.array.game_modes);
        gameModeValues = getResources().getStringArray(R.array.game_values);
        gameModeDescriptions = getResources().getStringArray(R.array.desc_game_modes);
        viewPager.setAdapter(new GamePagerAdapter(getSupportFragmentManager()));
    }

    private class GamePagerAdapter extends FragmentPagerAdapter {

        GamePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return GameModeFragment.newInstance(gameModes[position], gameModeValues[position], gameModeDescriptions[position]);
        }

        @Override
        public int getCount() {
            if (offline)
                return 4;
            else return 5;
        }
    }
}
