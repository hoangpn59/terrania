package net.volangvang.terrania.play.data;


import com.google.gson.annotations.SerializedName;

public class Answer {

    @SerializedName("correct_answer")
    private int correctAnswer;
    @SerializedName("recorded_answer")
    private int recordedAnswer;
    @SerializedName("current_score")
    private int currentScore;

    /* Return the answer play chose */
    public int getCorrectAnswer() {
        return correctAnswer;
    }

    /* Return the true answer*/
    public int getRecordedAnswer() {
        return recordedAnswer;
    }

    /* Get The Score*/
    public int getCurrentScore() {
        return currentScore;
    }

    public Answer(int correctAnswer, int recorded_answer) {
        this.correctAnswer = correctAnswer;
        this.recordedAnswer = recorded_answer;
        this.currentScore = 0;
    }
}
