package net.volangvang.terrania.play.data;

import com.google.gson.annotations.SerializedName;

public class GameID {
    @SerializedName("hashed_id")
    private String hashedId;
    public GameID(String hashed_id) {
        this.hashedId = hashed_id;
    }

    /* Return the game id */
    public String getHashedId() {
        return hashedId;
    }
}
