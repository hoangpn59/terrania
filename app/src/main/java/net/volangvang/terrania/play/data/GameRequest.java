package net.volangvang.terrania.play.data;

import com.google.gson.annotations.SerializedName;

public class GameRequest {
    @SerializedName("type")
    private String type;
    @SerializedName("language")
    private String language;
    @SerializedName("question_count")
    private int questionCount;
    @SerializedName("continent")
    private String continent;


    /* Create a game with information */
    public GameRequest(String type, String language, int questionCount, String continent) {
        this.type = type;
        this.language = language;
        this.questionCount = questionCount;
        this.continent = continent;
    }


}
