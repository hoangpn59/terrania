package net.volangvang.terrania;

import android.app.Dialog;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class AboutFragment extends DialogFragment {

    public AboutFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(R.layout.fragment_about)
                .setTitle(R.string.nav_about)
                .setPositiveButton(R.string.ok, null);
        return builder.create();
    }

    @Override
    public void onViewCreated(@NonNull View dialog, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(dialog, savedInstanceState);
        ((TextView) dialog.findViewById(R.id.body_acknowledgements)).setMovementMethod(new LinkMovementMethod());
        ((TextView) dialog.findViewById(R.id.body_team)).setMovementMethod(new LinkMovementMethod());
    }
}
