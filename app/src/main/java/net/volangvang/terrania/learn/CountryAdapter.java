package net.volangvang.terrania.learn;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.volangvang.terrania.R;
import net.volangvang.terrania.data.CountryContract;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {

    private Cursor cursor;
    private Activity activity;

    CountryAdapter(Activity activity) {
        this.activity = activity;
    }

    void swapCursor(Cursor cursor) {
        this.cursor = cursor;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_country, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.moveToPosition(position);
            int countryColumn = cursor.getColumnIndex(Locale.getDefault().getLanguage().equals("vi") ?
                    CountryContract.CountryEntry.COLUMN_NAME_VI : CountryContract.CountryEntry.COLUMN_NAME);
            final int id = cursor.getInt(cursor.getColumnIndex(CountryContract.CountryEntry._ID));
            String name = cursor.getString(countryColumn);
            String code = cursor.getString(cursor.getColumnIndex(CountryContract.CountryEntry.COLUMN_COUNTRY_CODE));
            holder.countryName.setText(name);
            int imgId = activity.getResources().getIdentifier("country_" + code.toLowerCase(), "drawable", activity.getPackageName());
            Picasso.get().load(imgId).into(holder.countryFlag);
            holder.itemView.setOnClickListener(view -> {
                Intent intent = new Intent(activity, CountryActivity.class);
                intent.putExtra("id", id);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.countryFlag.setTransitionName("image_transition");
                    Pair<View, String> pair = Pair.create(holder.countryFlag, "image_transition");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, pair);
                    activity.startActivity(intent, options.toBundle());
                }
                else {
                    activity.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.country_name)
        TextView countryName;
        @BindView(R.id.country_flag)
        ImageView countryFlag;
        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
