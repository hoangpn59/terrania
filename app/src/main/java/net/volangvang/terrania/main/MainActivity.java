package net.volangvang.terrania.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.images.ImageManager;
import com.google.android.gms.games.AchievementsClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.LeaderboardsClient;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayersClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import net.volangvang.terrania.AboutFragment;
import net.volangvang.terrania.R;
import net.volangvang.terrania.learn.LearnActivity;
import net.volangvang.terrania.play.PlayActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.btn_play_now) Button btnPlayNow;
    @BindView(R.id.layout_not_signed_in) View layoutNotSignedIn;
    @BindView(R.id.layout_signed_in) View layoutSignedIn;
    @BindView(R.id.text_signed_in) TextView textSignedIn;
    @BindView(R.id.switch_sound) SwitchCompat switchSound;
    @BindView(R.id.switch_offline) SwitchCompat switchOffline;
    TextView prompt;
    TextView textUserName;
    ImageView userImg;
    ImageView userBanner;
    private SharedPreferences preferences;
    private static int RC_SIGN_IN = 2948;
    private AchievementsClient achievementsClient;
    private LeaderboardsClient leaderboardsClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        View header = navigationView.getHeaderView(0);
        prompt = header.findViewById(R.id.login_logout_prompt);
        textUserName = header.findViewById(R.id.user_name);
        userImg = header.findViewById(R.id.user_image);
        userBanner = header.findViewById(R.id.user_banner);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);
        btnPlayNow.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, PlayActivity.class);
            startActivity(intent);
        });
        header.setOnClickListener(view -> onSignOutClicked());

        switchSound.setChecked(preferences.getBoolean("sound", false));
        switchSound.setOnCheckedChangeListener((compoundButton, b) -> preferences.edit().putBoolean("sound", b).apply());
        switchOffline.setChecked(preferences.getBoolean("offline", true));
        switchOffline.setOnCheckedChangeListener((compoundButton, b) -> preferences.edit().putBoolean("offline", b).apply());
    }

    private void signInSilently() {
        GoogleSignInClient signInClient = GoogleSignIn.getClient(this,
                GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
        signInClient.silentSignIn().addOnCompleteListener(task -> {
            if (task.isSuccessful()) onConnected(task.getResult());
            else signInExplicitly();
        });
    }

    private void signInExplicitly() {
        GoogleSignInClient signInClient = GoogleSignIn.getClient(this,
                GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
        Intent intent = signInClient.getSignInIntent();
        startActivityForResult(intent, RC_SIGN_IN);
    }

    private void onConnected(GoogleSignInAccount account) {
        // Get clients
        achievementsClient = Games.getAchievementsClient(this, account);
        leaderboardsClient = Games.getLeaderboardsClient(this, account);
        PlayersClient playersClient = Games.getPlayersClient(this, account);
        // We have to use this to load images.
        final ImageManager manager = ImageManager.create(this);
        playersClient.getCurrentPlayer().addOnCompleteListener(new OnCompleteListener<Player>() {
            @Override
            public void onComplete(@NonNull Task<Player> task) {
                Player player = task.getResult();
                textUserName.setText(player.getDisplayName());
                textSignedIn.setText(getString(R.string.msg_signed_in, player.getDisplayName()));
                prompt.setText(getString(R.string.msg_profile_prompt));
                layoutNotSignedIn.setVisibility(View.GONE);
                layoutSignedIn.setVisibility(View.VISIBLE);
                manager.loadImage(userImg, player.getIconImageUri());
                Uri bannerUri = (getResources().getBoolean(R.bool.is_landscape))?
                        player.getBannerImageLandscapeUri() : player.getBannerImagePortraitUri();
                manager.loadImage(userBanner, bannerUri);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!signInFlow && !explicitSignOut) {
            signInSilently();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_learn) {
            Intent intent = new Intent(this, LearnActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_play) {
            Intent intent = new Intent(this, PlayActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_about) {
            DialogFragment dialog = new AboutFragment();
            dialog.show(getSupportFragmentManager(), null);
        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private boolean explicitSignOut = false;
    boolean signInFlow = false; // in the middle of the sign-in flow

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);
            if (result.isSuccess()) {
                onConnected(result.getSignInAccount());
            }
            else {
                if (result.getStatus().getStatusMessage() == null ||
                        result.getStatus().getStatusMessage().isEmpty()) {
                    Toast.makeText(this, R.string.msg_sign_in_error, Toast.LENGTH_SHORT).show();
                    explicitSignOut = true;
                    showSignInButton();
                }
            }
        }
    }

    private void showSignInButton() {
        // Display the sign-in button
        layoutNotSignedIn.setVisibility(View.VISIBLE);
        layoutSignedIn.setVisibility(View.GONE);
        prompt.setText(R.string.msg_not_signed_in);
        textUserName.setText(R.string.app_name);
        Picasso.get()
                .load(R.mipmap.ic_launcher)
                .into(userImg);
        Picasso.get()
                .load(R.drawable.bg_nav)
                .into(userBanner);
    }


    @OnClick (R.id.btn_sign_in)
    public void onSignInClicked() {
        signInSilently();
    }

    @OnClick (R.id.btn_sign_out)
    public void onSignOutClicked() {
        explicitSignOut = true; // turn off automatic sign-in
        GoogleSignInClient signInClient = GoogleSignIn.getClient(this,
                GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
        signInClient.signOut().addOnCompleteListener(this, task -> showSignInButton());
    }

    @OnClick (R.id.btn_achievements)
    public void onAchievementsClicked() {
        achievementsClient.getAchievementsIntent().addOnCompleteListener(task ->
                startActivityForResult(task.getResult(), 5312));
    }

    @OnClick (R.id.btn_leaderboards)
    public void onLeaderboardsClicked() {
        leaderboardsClient.getAllLeaderboardsIntent().addOnCompleteListener(task ->
                startActivityForResult(task.getResult(), 2391));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
